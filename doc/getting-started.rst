###############
Getting Started
###############

Installation
============

Installation is as simple as::

    pip install skeletonizer

Contribute
==========

You can contribute to this code through Pull Request on GitLab_. Please, make
sure that your code is coming with unit tests to ensure full coverage and
continuous integration in the API.

.. _GitLab: https://gitlab.com/deltares/skeletonizer/merge_requests
