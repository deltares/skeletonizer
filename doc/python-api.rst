Python API
==========

Network
-------

.. automodule:: skeletonizer.network
    :members:
    :member-order: bysource
    :special-members: __init__
    :show-inheritance:
