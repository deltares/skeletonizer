import unittest

from skeletonizer.network import Network, Node, Pipe
from skeletonizer.network import ParallelMergeOperationFailed, SerialMergeOperationFailed


class SerialNetworkTest(unittest.TestCase):

    def setUp(self):
        # A simple network with two serial pipes of different length, and
        # three equal nodes.
        self.network = Network()
        self.n1 = Node("N1")
        self.n2 = Node("N2")
        self.n3 = Node("N3")

        self.p1 = Pipe("P1", self.n1, self.n2, 100, 200, 0.1)
        self.p2 = Pipe("P2", self.n2, self.n3, 300, 200, 0.1)

        self.network.add_nodes(self.n1, self.n2, self.n3)
        self.network.add_pipes(self.p1, self.p2)

    def test_is_serial(self):
        self.assertTrue(self.network.is_serial(self.p1, self.p2))

    def test_is_parallel(self):
        self.assertFalse(self.network.is_parallel(self.p1, self.p2))

    def test_merge_serial(self):
        self.assertIn(self.n2, self.network.nodes)

        # Merge serial pipes
        self.assertEqual(self.n3.pipes[0], self.p2)
        self.network.merge_serial(self.p1, self.p2)
        self.assertEqual(self.n3.pipes[0], self.p1)

        self.assertEqual(len(self.network.nodes), 2)
        self.assertEqual(len(self.network.pipes), 1)

        self.assertNotIn(self.n2, self.network.nodes)
        self.assertEqual(self.network.pipes[0].length, 400)
        self.assertEqual(self.network.pipes[0].diameter, 200)

    def test_merge_serial_demand_distribution(self):
        self.n1.demand = 1.0
        self.n2.demand = 2.0
        self.n3.demand = 3.0

        self.assertIn(self.n2, self.network.nodes)

        # Merge serial pipes
        self.network.merge_serial(self.p1, self.p2)

        # Flow is distributed with inverse ratio to pipe length
        self.assertEqual(self.n1.demand, 1.0 + 0.75 * 2.0)
        self.assertEqual(self.n3.demand, 3.0 + 0.25 * 2.0)

    def test_merge_parallel(self):
        with self.assertRaises(ParallelMergeOperationFailed):
            self.network.merge_parallel(self.p1, self.p2)


class ParallelNetworkTest(unittest.TestCase):

    def setUp(self):
        # A simple network with two parallel pipes of different length, and
        # two equal nodes.
        self.network = Network()
        self.n1 = Node("N1")
        self.n2 = Node("N2")

        self.p1 = Pipe("P1", self.n1, self.n2, 100, 100, 0.1)
        self.p2 = Pipe("P2", self.n1, self.n2, 50, 200, 0.5)

        self.network.add_nodes(self.n1, self.n2)
        self.network.add_pipes(self.p1, self.p2)

    def test_is_serial(self):
        self.assertFalse(self.network.is_serial(self.p1, self.p2))

    def test_is_parallel(self):
        self.assertTrue(self.network.is_parallel(self.p1, self.p2))

    def test_merge_parallel(self):
        self.assertEqual(len(self.network.pipes), 2)

        # Merge parallel pipes
        self.assertEqual(self.n2.pipes[0], self.p1, self.p2)
        self.network.merge_parallel(self.p1, self.p2)
        self.assertEqual(self.n2.pipes[0], self.p1)

        self.assertEqual(len(self.network.nodes), 2)
        self.assertEqual(len(self.network.pipes), 1)

        self.assertAlmostEqual(self.network.pipes[0].length, 66.66666666667)
        self.assertAlmostEqual(self.network.pipes[0].diameter, 223.60679774997897)

    def test_merge_serial(self):
        with self.assertRaises(SerialMergeOperationFailed):
            self.network.merge_serial(self.p1, self.p2)


class ProfileSerialPipeTest(unittest.TestCase):

    def setUp(self):
        # A simple network with two parallel pipes of different length, and
        # two equal nodes.
        self.network = Network()
        self.n1 = Node("N1", 0.0, 0.0)
        self.n2 = Node("N2", 1.0, 0.0)
        self.n3 = Node("N3", 2.0, 0.0)

        self.network.add_nodes(self.n1, self.n2, self.n3)

    def test_profile_right_right(self):
        # Both pipes have the same direction, with the p2 being 'downstream'
        # of p1: --p1--> --p2-->
        p1 = Pipe("P1", self.n1, self.n2, 100, 200, 0.5, [(30, 2), (45, 3)])
        p2 = Pipe("P2", self.n2, self.n3, 50, 200, 0.5, [(20, 3), (25, 4)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.profile, [(30, 2), (45, 3), (100, 1), (120, 3), (125, 4)])
        self.assertEqual(p1.start_node, merged_pipe.start_node)
        self.assertEqual(p2.end_node, merged_pipe.end_node)

    def test_profile_right_left(self):
        # The pipes are pointing towards each other: --p1--> <--p2--
        p1 = Pipe("P1", self.n1, self.n2, 100, 200, 0.5, [(30, 2), (45, 3)])
        p2 = Pipe("P2", self.n3, self.n2, 50, 200, 0.5, [(20, 3), (25, 4)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.profile, [(30, 2), (45, 3), (100, 1), (125, 4), (130, 3)])
        self.assertEqual(p1.start_node, merged_pipe.start_node)
        self.assertEqual(p2.end_node, merged_pipe.end_node)

    def test_profile_left_right(self):
        # The pipes are pointing away from each other: <--p1-- --p2-->
        p1 = Pipe("P1", self.n2, self.n1, 100, 200, 0.5, [(30, 2), (45, 3)])
        p2 = Pipe("P2", self.n2, self.n3, 50, 200, 0.5, [(20, 3), (25, 4)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.profile, [(25, 4), (30, 3), (50, 1), (80, 2), (95, 3)])
        self.assertEqual(p1.end_node, merged_pipe.end_node)
        self.assertEqual(p2.end_node, merged_pipe.start_node)

    def test_profile_left_left(self):
        # The pipes are pointing in the same direction, with p1 'downstream'
        # from p2: <--p1-- <--p2--
        p1 = Pipe("P1", self.n2, self.n1, 100, 200, 0.5, [(30, 2), (45, 3)])
        p2 = Pipe("P2", self.n3, self.n2, 50, 200, 0.5, [(20, 3), (25, 4)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.profile, [(20, 3), (25, 4), (50, 1), (80, 2), (95, 3)])
        self.assertEqual(p1.start_node, merged_pipe.start_node)
        self.assertEqual(p2.end_node, merged_pipe.start_node)


class ProfileParallelPipeTest(unittest.TestCase):

    def setUp(self):
        # A simple network with two parallel pipes of different length, and
        # two equal nodes.
        self.network = Network()
        self.n1 = Node("N1", 0.0, 0.0)
        self.n2 = Node("N2", 1.0, 0.0)

        self.network.add_nodes(self.n1, self.n2)

    def test_profile(self):
        p1 = Pipe("P1", self.n1, self.n2, 50, 200, 0.5, [(30, 3), (45, 3), (55, 8)])
        p2 = Pipe("P2", self.n1, self.n2, 100, 200, 0.5, [(30, 2), (45, 4)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_parallel(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.profile, [(40, 3), (60.00000000000001, 3), (73.33333333333334, 8)])


class VerticesSerialPipeTest(unittest.TestCase):
    def setUp(self):
        # A simple network with two serial pipes of different length,
        # different horizontal profiles (vertices)
        self.network = Network()
        self.n1 = Node("N1", 0.0, 0.0, (0.0, 0.0))
        self.n2 = Node("N2", 1.0, 0.0, (1.0, 6.0))
        self.n3 = Node("N3", 2.0, 0.0, (3.0, 8.0))
        self.network.add_nodes(self.n1, self.n2, self.n3)

    # Definition of right and left in the tests below is the same as in the
    # tests of ProfileSerialPipeTest
    def test_vertices_right_right(self):
        p1 = Pipe("P1", self.n1, self.n2, 100, 200, 0.5, [], [(0.5, 2), (0.8, 4)])
        p2 = Pipe("P2", self.n2, self.n3, 50, 200, 0.5, [], [(1.2, 1), (1.8, 9)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(self.p1, self.p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.vertices, [(0.5, 2), (0.8, 4), (1, 6), (1.2, 1), (1.8, 9)])

    def test_vertices_right_left(self):
        p1 = Pipe("P1", self.n1, self.n2, 100, 200, 0.5, [], [(0.5, 2), (0.8, 4)])
        p2 = Pipe("P2", self.n3, self.n2, 50, 200, 0.5, [], [(1.2, 1), (1.8, 9)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(self.p1, self.p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.vertices, [(0.5, 2), (0.8, 4), (1.0, 6.0), (1.8, 9), (1.2, 1)])

    def test_vertices_left_right(self):
        p1 = Pipe("P1", self.n2, self.n1, 100, 200, 0.5, [], [(0.5, 2), (0.8, 4)])
        p2 = Pipe("P2", self.n2, self.n3, 50, 200, 0.5, [], [(1.2, 1), (1.8, 9)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(self.p1, self.p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.vertices, [(1.8, 9), (1.2, 1), (1.0, 6.0), (0.5, 2), (0.8, 4)])

    def test_vertices_left_left(self):
        p1 = Pipe("P1", self.n2, self.n1, 100, 200, 0.5, [], [(0.5, 2), (0.8, 4)])
        p2 = Pipe("P2", self.n3, self.n1, 50, 200, 0.5, [], [(1.2, 1), (1.8, 9)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(self.p1, self.p2)
        self.network.merge_serial(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.vertices, [(0.5, 2), (0.8, 4), (0.0, 0.0), (1.8, 9), (1.2, 1)])


class VerticesParallelPipeTest(unittest.TestCase):
    def setUp(self):
        # A simple network with two serial pipes of different length,
        # different horizontal profiles (vertices)
        self.network = Network()
        self.n1 = Node("N1", 0.0, 0.0, (0.0, 0.0))
        self.n2 = Node("N2", 1.0, 0.0, (1.0, 6.0))
        self.network.add_nodes(self.n1, self.n2)

    def test_profile(self):
        p1 = Pipe("P1", self.n1, self.n2, 50, 200, 0.5, [], [(0.5, 2), (0.8, 4)])
        p2 = Pipe("P2", self.n1, self.n2, 100, 200, 0.5, [], [(1.2, 1), (1.8, 9)])
        self.p1 = p1
        self.p2 = p2
        self.network.add_pipes(p1, p2)
        self.network.merge_parallel(self.p1, self.p2)

        merged_pipe = self.network.pipes[0]
        self.assertListEqual(merged_pipe.vertices, [(0.5, 2), (0.8, 4)])


class BranchRemovalTest(unittest.TestCase):
    """ Check if there are branches in the provided network.
    The second part of the test is removing the branches and transfering
    the water hammer storage to the starting node of the branch.

              +
              |p5
         p1   |
      +-------+
      |       |
    p2|       |p4
      |       |
      +-------+
         p3   |
              |p6
              +
    """

    def setUp(self):
        # A network of 6 pipes
        self.network = Network()
        self.n0 = Node("N0", 0.0, 0.0, (0.0, 0.0))
        self.n1 = Node("N1", 0.0, 0.0, (0.0, 0.0))
        self.n2 = Node("N2", 1.0, 5.0, (1.0, 6.0))
        self.n3 = Node("N3", 1.0, 4.0, (1.0, 6.0))
        self.n4 = Node("N4", 1.0, 1.0, (1.0, 6.0))
        self.n5 = Node("N5", 1.0, 2.0, (1.0, 6.0))

        self.p1 = Pipe("P1", self.n0, self.n1, 50, 200, 0.5, [], [], 200)
        self.p2 = Pipe("P2", self.n1, self.n2, 100, 200, 0.5, [], [], 1200)
        self.p3 = Pipe("P3", self.n2, self.n3, 100, 1000, 0.5, [], [], 500)
        self.p4 = Pipe("P4", self.n3, self.n0, 100, 1000, 0.5, [], [], 500)
        # Pipe 5 has a direction from the dead end to the network
        self.p5 = Pipe("P5", self.n4, self.n0, 100, 1000, 0.5, [], [], 600)
        # Pipe 6 has a direction from the network to the dead end
        self.p6 = Pipe("P6", self.n3, self.n5, 100, 1000, 0.5, [], [], 500)
        self.network.add_nodes(self.n0, self.n1, self.n2, self.n3, self.n4, self.n5)
        self.network.add_pipes(self.p1, self.p2, self.p3, self.p4, self.p5, self.p6)

    def test_branch_search(self):
        self.assertTrue(self.network.is_branch(self.p5))
        self.assertTrue(self.network.is_branch(self.p6))
        self.assertFalse(self.network.is_branch(self.p1))
        self.assertFalse(self.network.is_branch(self.p2))
        self.assertFalse(self.network.is_branch(self.p3))
        self.assertFalse(self.network.is_branch(self.p4))

    def test_branch_collapse(self):
        s, e = self.p6.start_node, self.p6.end_node
        self.network.branch_collapse(self.p6)
        self.assertAlmostEqual(s.surge_storage, 30.819023931715872)
        self.assertEqual(s.demand, 6.0)
        self.assertNotIn(self.p6, self.network.pipes)
        self.assertNotIn(e, self.network.nodes)

        s, e = self.p5.start_node, self.p5.end_node
        self.network.branch_collapse(self.p5)
        self.assertAlmostEqual(e.surge_storage, 21.402099952580468)
        self.assertNotIn(self.p5, self.network.pipes)
        self.assertNotIn(s, self.network.nodes)


class ShortPipeRemovalTest(unittest.TestCase):
    r"""
    Check if there are short pipes in the provided network.
    If they are short pipes, remove them, merge their 2 nodes

     +          +
      \        /
    p1 \      / p2
        \    /
      n5 +--+ n6
        / p5 \
    p3 /      \ p4
      /        \
     +          +
    """
    def setUp(self):
        self.network = Network()
        self.n1 = Node("N1", 0.0, 1.0, (0.0, 0.0))
        self.n2 = Node("N2", 1.0, 2.0, (1.0, 6.0))
        self.n3 = Node("N3", 1.0, 3.0, (1.0, 6.0))
        self.n4 = Node("N4", 1.0, 4.0, (1.0, 6.0))
        self.n5 = Node("N5", 1.0, 5.0, (1.0, 6.0))
        self.n6 = Node("N6", 3.0, 6.0, (1.0, 6.0))

        self.p1 = Pipe("P1", self.n1, self.n5, 100, 200, 0.5, wave_speed=1000)
        self.p2 = Pipe("P2", self.n2, self.n6, 100, 200, 0.5, wave_speed=1000)
        self.p3 = Pipe("P3", self.n3, self.n5, 100, 200, 0.5, wave_speed=1000)
        self.p4 = Pipe("P4", self.n4, self.n6, 100, 200, 0.5, wave_speed=1000)
        self.p5 = Pipe("P5", self.n5, self.n6, 9, 200, 0.5, wave_speed=1000)

        self.network.add_nodes(self.n1, self.n2, self.n3, self.n4, self.n5, self.n6)
        self.network.add_pipes(self.p1, self.p2, self.p3, self.p4, self.p5)

    def test_short_pipe_removal(self):
        self.network.short_pipe_removal(self.p5)

        self.assertNotIn(self.n6, self.network.nodes)
        self.assertNotIn(self.p5, self.network.pipes)

        self.assertSetEqual(set(self.n5.pipes), {self.p1, self.p3, self.p2, self.p4})
        self.assertSetEqual(set(self.n5.pipes), set(self.network.pipes))

        self.assertEqual(self.n5.elevation, 2.0)
        self.assertEqual(self.n5.demand, 11.0)


class ShortPipeRemovalLoopTest(unittest.TestCase):
    r"""
    The short pipe to be removed (p2) is a loop.-


               p2
             +---+
        p1    \ /   p3
      +--------+-------+
     n1        n2      n3
    """

    def setUp(self):
        self.network = Network()
        self.n1 = Node("N1", 0.1, 1.0, (0.0, 0.0))
        self.n2 = Node("N2", 0.2, 2.0, (50.0, 0.0))
        self.n3 = Node("N3", 0.3, 3.0, (100.0, 0.0))

        self.p1 = Pipe("P1", self.n1, self.n2, 50, 200, 0.5, wave_speed=1000)
        self.p2 = Pipe("P2", self.n2, self.n2, 9,  200, 0.5, wave_speed=1000)
        self.p3 = Pipe("P3", self.n2, self.n3, 50, 200, 0.5, wave_speed=1000)

        self.network.add_nodes(self.n1, self.n2, self.n3)
        self.network.add_pipes(self.p1, self.p2, self.p3)

    def test_short_pipe_removal(self):
        self.network.short_pipe_removal(self.p2)

        self.assertNotIn(self.p2, self.network.pipes)
        self.assertNotIn(self.p2, self.n1.pipes)
        self.assertNotIn(self.p2, self.n2.pipes)
        self.assertNotIn(self.p2, self.n3.pipes)

        self.assertSetEqual(set(self.network.pipes), {self.p1, self.p3})
        self.assertSetEqual(set(self.network.nodes), {self.n1, self.n2, self.n3})

        self.assertEqual(self.n2.elevation, 0.2)
        self.assertEqual(self.n2.demand, 2.0)


if __name__ == "__main__":
    unittest.main()
