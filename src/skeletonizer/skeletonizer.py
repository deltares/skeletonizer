import logging
from typing import Callable

from .network import Pipe


logger = logging.getLogger('skeletonizer')


class Skeletonizer:
    def __init__(self, network):
        self.network = network

    def _parallel_r(self, func: Callable[[Pipe, Pipe], bool], **kwargs):
        candidates = set()
        candidate_pairs = set()

        # When building the list of candidates, no pipe can occur twice (e.g.
        # in one pair of parallel pipes, and another). Also be careful to do
        # an O(N) lookup of candidates, and not a naive O(N^2).
        for p in self.network.pipes:
            if p in candidates:
                continue
            parallel_pipes = set(p.end_node.pipes).intersection(p.start_node.pipes)
            parallel_pipes.remove(p)

            for a in parallel_pipes:
                if a in candidates:
                    continue
                if not self.network.can_merge_parallel(a, p):
                    continue

                candidate_pairs.add((a, p))
                candidates.update({a, p})

        for a, b in candidate_pairs:
            if func(a, b, **kwargs):
                self.network.merge_parallel(a, b)

    def _short_r(self, func: Callable[[Pipe], bool], **kwargs):
        candidates = [p for p in self.network.pipes if self.network.can_remove_short_pipe(p)]

        for p in candidates:
            if func(p, **kwargs):
                self.network.short_pipe_removal(p)

    def _branch_r(self, func: Callable[[Pipe], bool], **kwargs):
        candidates = [p for p in self.network.pipes if self.network.can_collapse_branch(p)]

        for p in candidates:
            if func(p, **kwargs):
                self.network.branch_collapse(p)

    def _serial_r(self, func: Callable[[Pipe, Pipe], bool], **kwargs):
        candidates = set()
        candidate_pairs = set()

        # When building the list of candidates, no pipe can occur twice (e.g.
        # in one pair of parallel pipes, and another). Also be careful to do
        # an O(N) lookup of candidates, and not a naive O(N^2).
        for p in self.network.pipes:
            if p in candidates:
                continue

            serial_pipes = set()

            if len(p.start_node.pipes) == 2 and not p.start_node.objects:
                serial_pipes.update({a for a in p.start_node.pipes if a is not p})

            if len(p.start_node.pipes) == 2 and not p.start_node.objects:
                serial_pipes.update({a for a in p.start_node.pipes if a is not p})

            for a in serial_pipes:
                if a in candidates:
                    continue
                if not self.network.can_merge_serial(a, p):
                    continue

                candidate_pairs.add((a, p))
                candidates.update({a, p})

        for a, b in candidate_pairs:
            if func(a, b, **kwargs):
                self.network.merge_serial(a, b)

    def _loop_call(self, f, max_loops, *args, **kwargs):
        """
        Repeatedly calls a function until the number of nodes and pipes in the
        network is constant, or the iteration count exceeds max_loops.
        """
        prev_size = (len(self.network.pipes), len(self.network.nodes))
        for _ in range(max_loops):
            f(*args, **kwargs)

            new_size = (len(self.network.pipes), len(self.network.nodes))
            if new_size == prev_size:
                break
            else:
                prev_size = new_size

    def log_network_statistics(func):
        def func_wrapper(self, *args, **kwargs):
            n_nodes_prev = len(self.network.nodes)
            n_pipes_prev = len(self.network.pipes)
            n_objects_prev = len(self.network.objects)

            func(self, *args, **kwargs)

            n_nodes = len(self.network.nodes)
            n_pipes = len(self.network.pipes)
            n_objects = len(self.network.objects)

            logger.info(func.__name__)
            logger.info("Nodes: %d -> %d  (%d)" % (n_nodes_prev, n_nodes, n_nodes_prev - n_nodes))
            logger.info("Pipes: %d -> %d  (%d)" % (n_pipes_prev, n_pipes, n_pipes_prev - n_pipes))
            logger.info("Objects: %d -> %d  (%d)" % (n_objects_prev, n_objects, n_objects_prev - n_objects))

        return func_wrapper

    @log_network_statistics
    def short_pipe_removal(self, func: Callable[[Pipe], bool], max_loops=100):
        self._loop_call(self._short_r, max_loops, func)

    @log_network_statistics
    def branch_collapse(self, func: Callable[[Pipe], bool], max_loops=100):
        self._loop_call(self._branch_r, max_loops, func)

    @log_network_statistics
    def serial_pipe_removal(self, func: Callable[[Pipe, Pipe], bool], max_loops=100):
        self._loop_call(self._serial_r, max_loops, func)

    @log_network_statistics
    def parallel_pipe_removal(self, func: Callable[[Pipe, Pipe], bool], max_loops=100):
        self._loop_call(self._parallel_r, max_loops, func)
