from math import inf, pi, sqrt
from typing import List, NewType, Tuple

# TODO: CMH or CMS? Preference to use SI internally


class NetworkOperationDenied(Exception):
    pass


class SerialMergeOperationFailed(NetworkOperationDenied):
    pass


class ParallelMergeOperationFailed(NetworkOperationDenied):
    pass


class BranchCollapseOperationFailed(NetworkOperationDenied):
    pass


class ShortPipeRemovalFailed(NetworkOperationDenied):
    pass


Head = NewType('Head', float)
Discharge = NewType('Discharge', float)
Area = NewType('Area', float)


class Node:
    def __init__(self, name: str, elevation: float = 0.0, demand: float = 0.0, coordinates=(0, 0),
                 surge_storage: float = 0.0, **kwargs):
        self.name = name
        self.elevation = elevation
        self.demand = demand
        self.coordinates = coordinates
        self.surge_storage = surge_storage

        self._pipes = []
        self._objects = []

    def can_remove(self) -> bool:
        """
        Any special properties of the node that make it not removable. This
        method is called from the can_remove_node() and from the can_remove_pipe()
        on the network level, which does other checks as well.

        :returns: Whether or not Node can be removed.
        """
        return True

    @property
    def objects(self) -> List['NetworkObject']:
        """
        List of network objects (other than pipes) to which this node is attached.
        """
        return self._objects

    @property
    def pipes(self) -> List['Pipe']:
        """
        List of pipes to which this node is attached.
        """
        return self._pipes

    # TODO: Should we move this to the network class? We don't have a merge
    # method for pipes, so why should we have one for nodes.
    def merge(self, n: 'Node', f=1.0) -> None:
        """
        Merge this node with another node.

        Add fraction f of demand of node n to this node's demand Override this
        function if you also want to do something special with elevation, or
        other properties you may have added.
        """
        self.demand += n.demand * f

    def __str__(self):
        return self.name


class NetworkObject:
    """
    Base class for any objects in the network.
    """

    def __init__(self, name, *nodes):
        self.name = name
        self._nodes = list(nodes)

    def can_remove(self) -> bool:
        return False

    def replace_node(self, node: Node, other: Node) -> None:
        i = self._nodes.index(node)
        self._nodes[i] = other

    @property
    def nodes(self) -> List[Node]:
        return self._nodes

    def __str__(self):
        return self.name


class SinglePortNetworkObject(NetworkObject):
    """
    Base class for any objects in the network that are connected to a single
    node.
    """

    def __init__(self, name, node):
        super().__init__(name, node)

    @property
    def node(self):
        return self._nodes[0]


class TwoPortNetworkObject(NetworkObject):
    """
    Base class for any objects in the network that are connected to two nodes.
    """

    def __init__(self, name, start_node, end_node):
        super().__init__(name, start_node, end_node)

    def other_node(self, n: Node):
        """
        Returns the node that is not equal to n
        """
        if n not in self._nodes:
            raise Exception("Node {} is not connected to this object.".format(n))
        else:
            return self.start_node if n is self.end_node else self.end_node

    def common_node(self, o: 'TwoPortNetworkObject'):
        common_nodes = set(self.nodes).intersection(set(o.nodes))
        if len(common_nodes) != 1:
            return None
        else:
            return common_nodes.pop()

    @property
    def start_node(self):
        return self._nodes[0]

    @property
    def end_node(self):
        return self._nodes[1]


class Tank(SinglePortNetworkObject):
    def __init__(self, name, node, elevation, initial_level, diameter, minimum_level=-inf, maximum_level=inf,
                 minimum_volume=0.0):
        super().__init__(name, node)
        self.elevation = elevation
        self.initial_level = initial_level
        self.diameter = diameter
        self.minimum_level = minimum_level
        self.maximum_level = maximum_level
        self.minimum_volume = minimum_volume


class Reservoir(SinglePortNetworkObject):
    def __init__(self, name, node, head):
        super().__init__(name, node)
        self.head = head


class Pump(TwoPortNetworkObject):
    pass


class Valve(TwoPortNetworkObject):
    def __init__(self, name, start_node, end_node, diameter, valve_type, opened=True):
        super().__init__(name, start_node, end_node)
        self.diameter = diameter
        self.valve_type = valve_type
        self.opened = opened


class Pipe(TwoPortNetworkObject):

    def __init__(self, name, start_node, end_node, length, diameter, roughness,
                 profile: List[Tuple[float, float]] = None, vertices: List[Tuple[float, float]] = None,
                 wave_speed: float = None, **kwargs):
        super().__init__(name, start_node, end_node)
        self.length = length
        self.diameter = diameter
        self.roughness = roughness
        self.vertices = vertices
        self.profile = profile
        self.wave_speed = wave_speed

        if vertices is None:
            vertices = []
        self.vertices = vertices

        if profile is None:
            profile = []
        self.profile = profile

    # Pipes can be removed by default
    def can_remove(self):
        return True

    def is_loop(self):
        return self.start_node is self.end_node

    def flip(self):
        """
        Flips the direction of the pipe, i.e. switching the nodes. Profile,
        vertices, and other properties of the pipe are switched accordingly.
        """
        self._nodes = self._nodes[::-1]
        self.profile = [(self.length - s, e) for s, e in self.profile][::-1]
        self.vertices = self.vertices[::-1]

    @property
    def surge_storage(self) -> float:
        """ Calculates water hammer storage per pipe element"""
        return 9.81 * (pi * self.diameter**2 / 4) / self.wave_speed**2


class Network:
    # TODO: Make sure that nodes and pipes can only be accessed through
    # iterators and other functions that can be overridden
    def __init__(self):
        # Make sure that we are working with ordered dicts, to keep things deterministic
        self._nodes = []
        self._pipes = []
        self._objects = []

    # Node methods
    @property
    def nodes(self):
        return self._nodes

    def add_nodes(self, *nodes):
        for n in nodes:
            self._nodes.append(n)

    def can_remove_node(self, n):
        # Examples include nodes connected to special pipes/valves/special components
        # or nodes with a special name prefix.
        if n.can_remove():
            return True
        else:
            return False

    # General object methods (not pipe specific)
    @property
    def objects(self):
        return self._objects

    def add_objects(self, *objects: List[NetworkObject]):
        for o in objects:
            if isinstance(o, Pipe):
                raise ValueError("Pipes should be added using the add_pipes() method.")
            self._objects.append(o)
            # An object may have the same start/end node; make sure to only
            # add it once in that case.
            for x in set(o._nodes):
                x._objects.append(o)

    # Pipe methods
    @property
    def pipes(self):
        return self._pipes

    def add_pipes(self, *pipes: List[Pipe]):
        for p in pipes:
            self._pipes.append(p)
            # A pipe may have the same start/end node; make sure to only add
            # it once in that case.
            for x in set(p._nodes):
                x._pipes.append(p)

    def wave_speed_from_roughness(self, mapping, overwrite=False):
        """For user provided wave speed, roughness function"""
        for p in self._pipes:
            if p.wave_speed is not None:
                overwrite
            try:
                p.wave_speed = mapping[p.roughness]
            except KeyError:
                raise Exception("Roughness not found")

    def can_remove_pipe(self, p: Pipe):
        if p.can_remove():
            return True
        else:
            return False

    def is_serial(self, a: Pipe, b: Pipe):
        common = a.common_node(b)

        if common is None:
            return False
        elif len(common.pipes) > 2:
            return False
        elif a.is_loop() or b.is_loop():
            return False
        else:
            return True

    def is_branch(self, a: Pipe):
        # Check if the examined pipe is a branch
        if len(a.start_node.pipes) == 1 and len(a.start_node.objects) == 0:
            return True
        elif len(a.end_node.pipes) == 1 and len(a.end_node.objects) == 0:
            return True
        else:
            return False

    def is_parallel(self, a: Pipe, b: Pipe) -> bool:
        # Check if they have exactly two nodes in common
        common = set(a.nodes).intersection(set(b.nodes))
        if len(common) != 2:
            return False
        elif a.is_loop() or b.is_loop():
            return False
        else:
            return True

    def can_merge_serial(self, a: Pipe, b: Pipe):
        common = a.common_node(b)

        if not self.is_serial(a, b):
            return False
        elif not self.can_remove_node(common):
            return False
        elif not self.can_remove_pipe(a) or not self.can_remove_pipe(b):
            return False
        else:
            return True

    def can_collapse_branch(self, a: Pipe):
        if not self.is_branch(a):
            return False
        elif not self.can_remove_pipe(a):
            return False
        else:
            return True

    def can_remove_short_pipe(self, a: Pipe):
        if not self.can_remove_pipe(a):
            return False
        elif not self.can_remove_node(a.start_node):
            return False
        elif not self.can_remove_node(a.end_node):
            return False
        else:
            return True

    def can_merge_parallel(self, a: Pipe, b: Pipe):
        if not self.is_parallel(a, b):
            return False
        elif not self.can_remove_pipe(a) or not self.can_remove_pipe(b):
            return False
        else:
            return True

    def merge_serial(self, a: Pipe, b: Pipe) -> Tuple[Node, Pipe]:
        """
        Tries to merge two serial pipes in the network.

        :returns: The common node and pipe that have been removed from the network.

        .. caution::

            - Changes in diameter cause reflections in transient analysis.
              Merging pipes into one removes these reflections and as such
              changes results.
            - If the demand of the common node is relatively large, a large
              portion of the flow will now go through both pipes or instead none
              at all (demand is spread to the edges). This can result in
              different head losses (and transient behavior as well).
            - Some output methods do not support pipe profile. So if the common
              node is located a lot higher than the outer nodes, a possible
              critical point in the system (low pressure/cavitation) can be
              missed.
        """
        common = a.common_node(b)

        if common is None:
            raise SerialMergeOperationFailed(
                "Tried to serially merge two pipes that are not connected.")

        if not self.is_serial(a, b):
            raise SerialMergeOperationFailed(
                "Tried to serially merge two pipes that are not serial pipes.")

        if not self.can_merge_serial(a, b):
            raise SerialMergeOperationFailed(
                "Tried to serially merge two pipes that are not allowed to be merged.")

        node_a = a.other_node(common)  # Node unique to pipe a
        node_b = b.other_node(common)  # Node unique to pipe b

        # Merge demands. The longer the pipe, the less demand
        demandf = a.length / (a.length + b.length)
        node_a.merge(common, 1.0 - demandf)
        node_b.merge(common, demandf)

        # Preserve length
        new_length = a.length + b.length
        # Preserve volume
        new_diam = sqrt((a.length * a.diameter**2 + b.length * b.diameter**2)/new_length)
        # length weighted k/D
        new_roughness = ((a.roughness * a.length / a.diameter + b.roughness * b.length / b.diameter)
                         * new_diam/new_length)

        # All merging code is written for pipe b being 'downstream' of pipe a.
        # Flip pipes if this is not the case.
        unflip = False
        if common is a.start_node:
            a.flip()
            unflip = True
        if common is b.end_node:
            b.flip()

        # Add removed node's height and other pipe's profile to profile
        new_profile = a.profile.copy()
        new_profile.append((a.length, common.elevation))
        new_profile.extend([(s + a.length, e) for s, e in b.profile])

        new_vertices = a.vertices.copy()
        new_vertices.append(common.coordinates)
        new_vertices.extend(b.vertices)

        # Set the new values
        a.length = new_length
        a.diameter = new_diam
        a.roughness = new_roughness
        a.profile = new_profile
        a.vertices = new_vertices

        a.replace_node(common, node_b)
        self._nodes.remove(common)
        self._pipes.remove(b)
        a.end_node._pipes.remove(b)
        a.end_node._pipes.append(a)

        if unflip:
            a.flip()

        # TODO: Should we also return the original a?
        # TODO: Don't forget to copy vertices in epanet
        return common, b

    def merge_parallel(self, a: Pipe, b: Pipe) -> Pipe:
        """
        Tries to merge two parallel pipes in the network.

        :returns: The pipe that has been removed from the network.

        .. caution::

            - If length differs by a lot, travel time of shock wave probably
              differs as well

            - Equivalent diameter of two short parallel pipes might be too
              large to do serial merging with afterwards
        """
        if not self.is_parallel(a, b):
            raise ParallelMergeOperationFailed(
                "Tried to parallel merge two pipes that are not parallel pipes.")

        if not self.can_merge_parallel(a, b):
            raise ParallelMergeOperationFailed(
                "Tried to parallel merge two pipes that are not allowed to be merged.")

        new_profile_a = [(s/a.length, e) for s, e in a.profile]
        new_profile_b = [(s/b.length, e) for s, e in b.profile]

        max_a = max((e for s, e in a.profile), default=-inf)
        max_b = max((e for s, e in b.profile), default=-inf)

        new_profile_norm = new_profile_a if max_a >= max_b else new_profile_b

        # Keep total surface area the same
        diameter = sqrt(a.diameter**2 + b.diameter**2)

        # Harmonically average the length and roughness (k-value)
        length = 1.0/((1.0/a.length + 1.0/b.length)/2)
        roughness = 1.0/((1.0/a.roughness + 1.0/b.roughness)/2)

        # Set the new values
        a.length = length
        a.roughness = roughness
        a.diameter = diameter
        a.profile = [(s*a.length, e) for s, e in new_profile_norm]
        a.vertices = a.vertices

        self._pipes.remove(b)
        a.start_node._pipes.remove(b)
        a.end_node._pipes.remove(b)

        # TODO: Don't forget to remove vertices in epanet
        return b

    def branch_collapse(self, a: Pipe):
        """
        Removes branch from the network and saves its surge storage
        to the common node.
        """

        if not self.is_branch(a):
            raise BranchCollapseOperationFailed(
                "Tried to collapse a pipe that is not a branch.")

        if not self.can_collapse_branch(a):
            raise BranchCollapseOperationFailed(
                "Tried to collapse a branch that is not allowed to be collapsed.")

        # Check if pipe is oriented the right way
        if len(a.start_node.pipes) == 1:
            a.flip()

        a.start_node.demand += a.end_node.demand
        a.start_node.surge_storage += a.surge_storage

        self._pipes.remove(a)
        self._nodes.remove(a.end_node)
        a.start_node._pipes.remove(a)

        return a, a.end_node

    def short_pipe_removal(self, a: Pipe):
        """
        Removes (short) pipe of the network, and merges the start node and end
        node into one. The adjectent pipes are _not_ changed.
        """
        if not self.can_remove_short_pipe(a):
            raise ShortPipeRemovalFailed(
                "Tried to remove a pipe that is not allowed to be removed.")

        loop = a.is_loop()

        if not loop:
            a.start_node.demand += a.end_node.demand
            a.start_node.elevation = (a.start_node.elevation + a.end_node.elevation)/2

        a.start_node.surge_storage += a.surge_storage

        # Bookkeeping
        a.start_node._pipes.remove(a)
        if not loop:
            a.end_node._pipes.remove(a)
            for p in a.end_node.pipes:
                p.replace_node(a.end_node, a.start_node)

            a.start_node._pipes.extend(a.end_node._pipes)
            self._nodes.remove(a.end_node)

        self._pipes.remove(a)

        return a, a.end_node
